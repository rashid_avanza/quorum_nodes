#!/bin/bash
set -u
set -e

GLOBAL_ARGS="--raft --rpc --rpcaddr 0.0.0.0 --rpcapi admin,db,eth,debug,miner,net,shh,txpool,personal,web3,quorum --emitcheckpoints"
WEBSOCKET_ARGS="--ws --wsaddr 0.0.0.0"
ROOT_DIR="/usr/local/nodes"
echo "[*] Starting Constellation nodes"
nohup constellation-node $ROOT_DIR/tm1.conf 2>> $ROOT_DIR/qdata/logs/constellation1.log &
sleep 1
nohup constellation-node $ROOT_DIR/tm2.conf 2>> $ROOT_DIR/qdata/logs/constellation2.log &
nohup constellation-node $ROOT_DIR/tm3.conf 2>> $ROOT_DIR/qdata/logs/constellation3.log &
nohup constellation-node $ROOT_DIR/tm4.conf 2>> $ROOT_DIR/qdata/logs/constellation4.log &
nohup constellation-node $ROOT_DIR/tm5.conf 2>> $ROOT_DIR/qdata/logs/constellation5.log &
nohup constellation-node $ROOT_DIR/tm6.conf 2>> $ROOT_DIR/qdata/logs/constellation6.log &
nohup constellation-node $ROOT_DIR/tm7.conf 2>> $ROOT_DIR/qdata/logs/constellation7.log &

sleep 1

echo "[*] Starting node 1 (permissioned)"
PRIVATE_CONFIG=tm1.conf nohup geth --datadir $ROOT_DIR/qdata/dd1 $GLOBAL_ARGS $WEBSOCKET_ARGS --wsorigins "*" --wsport 8546 --permissioned  --raftport 50401 --rpcport 22000 --port 21000 --unlock 0 --password passwords.txt 2>>$ROOT_DIR/qdata/logs/1.log &

echo "[*] Starting node 2 (permissioned)"
PRIVATE_CONFIG=tm2.conf nohup geth --datadir $ROOT_DIR/qdata/dd2 $GLOBAL_ARGS $WEBSOCKET_ARGS --wsorigins "*" --wsport 8547 --permissioned --raftport 50402 --rpcport 22001 --port 21001 2>>$ROOT_DIR/qdata/logs/2.log &

echo "[*] Starting node 3 (permissioned)"
PRIVATE_CONFIG=tm3.conf nohup geth --datadir $ROOT_DIR/qdata/dd3 $GLOBAL_ARGS $WEBSOCKET_ARGS --wsorigins "*" --wsport 8548 --permissioned --raftport 50403 --rpcport 22002 --port 21002 2>>$ROOT_DIR/qdata/logs/3.log &

echo "[*] Starting node 4 (permissioned)"
PRIVATE_CONFIG=tm4.conf nohup geth --datadir $ROOT_DIR/qdata/dd4 $GLOBAL_ARGS $WEBSOCKET_ARGS --wsorigins "*" --wsport 8549 --permissioned --raftport 50404 --rpcport 22003 --port 21003 2>>$ROOT_DIR/qdata/logs/4.log &

echo "[*] Starting node 5 (unpermissioned)"
PRIVATE_CONFIG=tm5.conf nohup geth --datadir $ROOT_DIR/qdata/dd5 $GLOBAL_ARGS --raftport 50405 --rpcport 22004 --port 21004 2>>$ROOT_DIR/qdata/logs/5.log &

echo "[*] Starting node 6 (unpermissioned)"
PRIVATE_CONFIG=tm6.conf nohup geth --datadir $ROOT_DIR/qdata/dd6 $GLOBAL_ARGS --raftport 50406 --rpcport 22005 --port 21005 2>>$ROOT_DIR/qdata/logs/6.log &

echo "[*] Starting node 7 (unpermissioned)"
PRIVATE_CONFIG=tm7.conf nohup geth --datadir $ROOT_DIR/qdata/dd7 $GLOBAL_ARGS --raftport 50407 --rpcport 22006 --port 21006 2>>$ROOT_DIR/qdata/logs/7.log &

echo "[*] Waiting for nodes to start"
sleep 10
echo "[*] Sending first transaction"
PRIVATE_CONFIG=$ROOT_DIR/tm1.conf geth --exec 'loadScript("/usr/local/nodes/script1.js")' attach ipc:$ROOT_DIR/qdata/dd1/geth.ipc

echo "All nodes configured. See 'qdata/logs' for logs, and run e.g. 'geth attach qdata/dd1/geth.ipc' to attach to the first Geth node"
