#!/bin/bash
set -u
set -e

LOC='/usr/local/nodes'

echo "[*] Cleaning up temporary data directories"
rm -rf $LOC/qdata
mkdir -p $LOC/qdata/logs

echo "[*] Configuring node 1 (permissioned)"
mkdir -p $LOC/qdata/dd1/{keystore,geth}
cp $LOC/permissioned-nodes.json $LOC/qdata/dd1/static-nodes.json
cp $LOC/permissioned-nodes.json $LOC/qdata/dd1/
cp $LOC/keys/key1 $LOC/qdata/dd1/keystore
cp $LOC/raft/nodekey1 $LOC/qdata/dd1/geth/nodekey
geth --datadir $LOC/qdata/dd1 init $LOC/genesis.json

echo "[*] Configuring node 2 (permissioned)"
mkdir -p $LOC/qdata/dd2/{keystore,geth}
cp $LOC/permissioned-nodes.json $LOC/qdata/dd2/static-nodes.json
cp $LOC/permissioned-nodes.json $LOC/qdata/dd2/
cp $LOC/keys/key2 $LOC/qdata/dd2/keystore
cp $LOC/keys/key3 $LOC/qdata/dd2/keystore
cp $LOC/raft/nodekey2 $LOC/qdata/dd2/geth/nodekey
geth --datadir $LOC/qdata/dd2 init $LOC/genesis.json

echo "[*] Configuring node 3 (permissioned)"
mkdir -p $LOC/qdata/dd3/{keystore,geth}
cp $LOC/permissioned-nodes.json $LOC/qdata/dd3/static-nodes.json
cp $LOC/permissioned-nodes.json $LOC/qdata/dd3/
cp $LOC/raft/nodekey3 $LOC/qdata/dd3/geth/nodekey
geth --datadir $LOC/qdata/dd3 init $LOC/genesis.json

echo "[*] Configuring node 4 (permissioned)"
mkdir -p $LOC/qdata/dd4/{keystore,geth}
cp $LOC/permissioned-nodes.json $LOC/qdata/dd4/static-nodes.json
cp $LOC/permissioned-nodes.json $LOC/qdata/dd4/
cp $LOC/keys/key4 $LOC/qdata/dd4/keystore
cp $LOC/raft/nodekey4 $LOC/qdata/dd4/geth/nodekey
geth --datadir $LOC/qdata/dd4 init $LOC/genesis.json

echo "[*] Configuring node 5"
mkdir -p $LOC/qdata/dd5/{keystore,geth}
cp $LOC/permissioned-nodes.json $LOC/qdata/dd5/static-nodes.json
cp $LOC/keys/key5 $LOC/qdata/dd5/keystore
cp $LOC/raft/nodekey5 $LOC/qdata/dd5/geth/nodekey
geth --datadir $LOC/qdata/dd5 init $LOC/genesis.json

echo "[*] Configuring node 6"
mkdir -p $LOC/qdata/dd6/{keystore,geth}
cp $LOC/permissioned-nodes.json $LOC/qdata/dd6/static-nodes.json
cp $LOC/raft/nodekey6 $LOC/qdata/dd6/geth/nodekey
geth --datadir $LOC/qdata/dd6 init $LOC/genesis.json

echo "[*] Configuring node 7"
mkdir -p $LOC/qdata/dd7/{keystore,geth}
cp $LOC/permissioned-nodes.json $LOC/qdata/dd7/static-nodes.json
cp $LOC/raft/nodekey7 $LOC/qdata/dd7/geth/nodekey
geth --datadir $LOC/qdata/dd7 init $LOC/genesis.json
